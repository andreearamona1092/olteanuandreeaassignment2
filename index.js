

const FIRST_NAME = "Andreea Ramona";
const LAST_NAME = "Olteanu";
const GRUPA = "1092";
 
class cache{
    pageAccessCounter(stringValue){
        if(stringValue === undefined){
        if(this.hasOwnProperty('home'))
        {
             this["home"] += 1;
        }
        else{
            this["home"] = 1;
        }
        
        }
        else{
            stringValue = stringValue.toLowerCase();
            if(this.hasOwnProperty(stringValue)){
                this[stringValue] += 1;
            }
            else
            this[stringValue] = 1;
        }
    } 
    

    getCache(){
        return this;
    }

    
}
/**
 * Make the implementation here
 */
function initCaching() {
   let tema = new cache();

   return tema;
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

